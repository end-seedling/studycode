package com.test.product.mapper;

import com.test.product.model.ProductInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface ProductMapper {

    @Select("Select * from product_detail where id=#{id}")
    ProductInfo selectProductById(Integer id);
}
