package com.test.product.controller;

import com.test.product.model.ProductInfo;
import com.test.product.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/{productId}")
    public ProductInfo getProduct(@PathVariable("productId") Integer productId){
        // 方便观察这里将日志进行打印
        log.info("收到参数，productId: " + productId);
        return productService.selectProductById(productId);
    }
}
