public class code01 {

//    大数加法

    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 计算两个数之和
     * @param s string字符串 表示第一个整数
     * @param t string字符串 表示第二个整数
     * @return string字符串
     */
    public String solve (String s, String t) {
        // write code here
        // 这道题需要考虑很多问题，并不简单
        // 将两个字符串直接数字化后相加是绝对不行的，因为会出现 int 类型直接爆炸的情况
        // 所以需要将数字一一取出相加后，一步一步构造相关的 String 返回值

        // 首先，针对两个数组存在 “” 的情况进行处理
        if (s.length() == 0) {
            return t;
        }
        if (t.length() == 0) {
            return s;
        }

        // 判断结束后，这里需要创建出一个 build 实例
        StringBuilder build = new StringBuilder();

        // 这里我们以两个字符串的长度作为判断条件
        int a = s.length() - 1;
        int b = t.length() - 1;

        // 这个变量用来记录进位信息
        int flag = 0;

        while (a >= 0 || b >= 0) {
            int num1 = 0;
            int num2 = 0;

            num1 = a < 0 ? 0 : s.charAt(a) - '0';
            a--;
            num2 = b < 0 ? 0 : t.charAt(b) - '0';
            b--;
            // 这里需要加上进位值
            int sum = num1 + num2 + flag;

            // 将个位上的数值进行构造
            build.insert(0, sum % 10);

            // 使用 flag 将进位数记录
            flag = sum / 10;
        }


        // 循环结束后，将多余的进位值插入到组合的头部
        if (flag != 0) {
            build.insert(0, flag);
        }

        return build.toString();
    }
}
