package SelectSort;

/*
*   选择排序是相当直观的一种排序算法，它的算法原理非常简单：
*   首先，在未排序序列中找到最 小(大) 的元素，将其存放到排序序列的 起始位置。
*   之后，再在剩余的未排序的数组中找到最 小(大) 的元素，将其存放到 已排序序列的末尾
*/


import java.util.Arrays;

public class Select {

    public static void SelectSort (int[] arr) {
        // 这里要将数组中的元素排列有序，是需要在两次循环中来进行实现的
        for(int i = 0; i < arr.length; i++) {
            // 此时需要定义一个参数，将数组中最前面的值进行记录
            // 以便于后面找到的更小的值进行进行交换
            int Min = i;
            // 再次通过一个循环，在 i 位置之后去寻找到更符合条件的值
            for(int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[Min]) {
                    // 当后面元素的值大于当前元素
                    // 就对 Min 的值进行交换
                    Min = j;
                }
            }
            // 在 j 完成 无序序列的扫描过后，就可能会得到一个更小的值，此时进行交换即可
            if(i != Min) {
                swap(arr,i,Min);
            }
        }
    }

    public static void swap(int[] arr, int i, int min) {
        int tmp = arr[i];
        arr[i] = arr[min];
        arr[min] = tmp;
    }

    public static void main(String[] args) {
        int[] arr = {9,8,7,6,5,4,3,1,2};
        SelectSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
