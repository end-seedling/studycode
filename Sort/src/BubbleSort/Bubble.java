package BubbleSort;

import java.util.Arrays;

/*
    *       冒泡排序是重复的遍历要排序的序列，依次比较两个元素，如果顺序错误就将其进行交换
    *       最终执行到不能再交换为止
    */
public class Bubble {

    public static void BubbleSort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            // 在这里需要定义一个标志位
            // 主要是用来记录是否已经完成排序
            // 当然每次在执行外层循环时都会进行一次更新
            boolean flag = true;
            for (int j = 0; j < arr.length - i - 1; j++) {
                if(arr[j] > arr[j + 1]) {
                    // 如果 j 位置的值 大于 j+1 位置
                    // 此时进行交换操作
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                    // 当完成一次排序后就针对 flag 进行一次修改
                    flag = false;
                }
            }
            if(flag) {
                break;
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {9,8,7,6,5,2,4,1,3};
        BubbleSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
