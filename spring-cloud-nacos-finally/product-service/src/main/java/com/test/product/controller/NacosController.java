package com.test.product.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope   // 实现配置热更新，实时生效
@RestController
public class NacosController {
    // 获取配置项中包含的值
    @Value("${nacos.config}")
    private String nacosConfig;

    @RequestMapping("/getConfig")
    // 获取 Nacos 中的配置项
    public String getConfig() {
        return "从 Nacos 中获取配置项nacos.config：" + nacosConfig;
    }
}
