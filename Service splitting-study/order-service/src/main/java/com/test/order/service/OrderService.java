package com.test.order.service;

import com.test.order.mapper.OrderMapper;
import com.test.order.model.OrderInfo;
import com.test.order.model.ProductInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OrderService {
    @Autowired
    private OrderMapper orderMapper;
    // 将 config 中引入的依赖进行注入
    @Autowired
    private RestTemplate restTemplate;

    public OrderInfo selectOrderById(Integer orderId) {
        // 1.第一步，搜索当前 id 下的订单信息
        OrderInfo orderInfo = orderMapper.selectOrderById(orderId);
        // 2.第二步，通过当前的订单信息中的 productId 来获取订单信息
        String url = "http://127.0.0.1:9090/product/" + orderInfo.getProductId();
        // 通过 url 获取到的信息会被组织成 productInfo 的格式进行记录
        ProductInfo productInfo = restTemplate.getForObject(url, ProductInfo.class);
        // 4. 第四步，对当前的两部分信息进行合并操作
        orderInfo.setProductInfo(productInfo);

        // 返回重组后的信息到 controller 层
        return orderInfo;
    }
}
