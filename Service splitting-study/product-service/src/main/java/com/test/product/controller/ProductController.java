package com.test.product.controller;

import com.test.product.model.ProductInfo;
import com.test.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/{productId}")
    public ProductInfo getProduct(@PathVariable("productId") Integer productId){
        return productService.selectProductById(productId);
    }
}
