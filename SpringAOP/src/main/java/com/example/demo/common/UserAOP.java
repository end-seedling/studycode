package com.example.demo.common;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Aspect     // 切面
@Component      // 将该方法注入到 Spring 框架中
public class UserAOP {

    // 切点 (配置拦截规则)
    @Pointcut("execution(*  com.example.demo.controller.UserController.*(..))")
    public void pointcut(){
        System.out.println("具体的拦截规则");
    }

    // 前置通知
    @Before("pointcut()")
    public void doBefore(){
        System.out.println("执行了前置通知： " + LocalDateTime.now());
    }


    //后置通知
    @After("pointcut()")
    public void doAfter(){
        System.out.println("执行了后置通知" + LocalDateTime.now());
    }

    //环绕通知
    @Around("pointcut()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("开始执行环绕通知");
        Object obj =  joinPoint.proceed();
        System.out.println("结束环绕通知");
        return obj;
    }
}
