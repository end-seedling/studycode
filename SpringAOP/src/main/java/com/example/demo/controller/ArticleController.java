package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArticleController {
    @RequestMapping("/art/sayhi")
    public String art(){
        System.out.println("执行了 article 方法");
        return "hi article";
    }
}
