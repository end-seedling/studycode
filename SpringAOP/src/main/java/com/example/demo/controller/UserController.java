package com.example.demo.controller;

import com.example.demo.common.UserAOP;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserAOP userAOP;

    @RequestMapping("/user/sayhi")
    public String sayhi(){
        System.out.println("执行了 sayhi 方法");
        userAOP.pointcut();
        return "hi spring boot aop";
    }

    @RequestMapping("/user/login")
    public String login(){
        System.out.println("执行了 login 方法");
        return "do login";
    }
}
