package TestClasses;

import Utils.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class MyBlogListTest extends TestUtils {

    // 先引入驱动
    public static WebDriver driver = getChromeDriver();
    // 创建截图存储目录
    public static String pathname = "./src/main/screenshots/MyBlogListTest";

    @BeforeAll
    public static void beforeAll() {
        // 打开网页
        driver.get("http://localhost:8080/login.html");
    }

    // 不能直接进入其他页面，需要进行登录判断，这里就实现一个登录方法
    public void login() {
        // 进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 因为没新开一个页面都需要进行登录，这里要针对 个人文章列表页 进行测试，所以在前置工作中需要进行一下登录操作
        // 登录
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("12345");

        // 选中登录按钮进行点击
        driver.findElement(By.cssSelector("#submit")).click();

        // 进行一下隐式等待，确保成功登录
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    // 首先针对查看全文进行测试
    @Order(3)
    @Test
    public void testCheckContent() throws IOException {

        // 进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 首先，在列表页中选中要查看的文章，并且记录下当前文章的标题
        WebElement getTitle = driver.findElement(By.cssSelector("#artlist > div:nth-child(1) > div.title"));
        String title = getTitle.getText();

        // 选中查看全文按钮，并进行点击
        WebElement getButton = driver.findElement(By.cssSelector("#artlist > div:nth-child(1) > a:nth-child(4)"));
        getButton.click();

        // 进入文章后查看文章，同时获取文章标题，查看标题是否符合
        WebElement getContentTitle = driver.findElement(By.cssSelector("#title"));
        String contentTitle = getContentTitle.getText();

        // 针对两个 title 进行判断
        boolean flag = false;
        if (title.equals(contentTitle)) {
            flag = true;
        }

        System.out.println("查看文章后，文章标题是否符合预期：" + flag);

        // 截取图片
        saveScreenShot(driver, "文章详情页截图", pathname);

        // 截取之后返回 个人博客列表页
        driver.navigate().back();

        // 结束之后关闭浏览器
        closeBrowser(driver);
    }

    // 针对文章修改进行测试
    @Order(2)
    @Test
    public void testRevise() throws InterruptedException {

        // 进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // 选中修改按钮，并进行点击
        driver.findElement(By.cssSelector("#artlist > div:nth-child(1) > a:nth-child(5)")).click();

        // 进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 选中文章内容，修改其中的信息 （方便起见，这里修改一下文章标题）
        WebElement getContent = driver.findElement(By.cssSelector("#title"));
        // 获取当前文章标题
        String content = getContent.getText();

        // 清空标题内容
        getContent.clear();
        getContent.sendKeys("修改后的文章内容");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 针对修改进行截图
        try {
            saveScreenShot(driver, "文章修改后截图", pathname);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 点击发布
        driver.findElement(By.cssSelector("body > div.blog-edit-container > div.title > button")).click();
        // 强制等待
        sleep(500);

        // 处理 alert 事件
        Alert alert = driver.switchTo().alert();


        // 点击确认
        alert.accept();
    }

    // 针对文章删除进行测试
    @Order(1)
    @Test
    public void testDelete() throws InterruptedException {

        // 首先，进行登录操作
        login();

        // 进行一下截图，截取删除前的页面
        try {
            saveScreenShot(driver, "文章删除前截图",pathname);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 先来记录一下要删除的文章标题
        WebElement getTitle = driver.findElement(By.cssSelector("#artlist > div:nth-child(1) > div.title"));
        String title = getTitle.getText();

        // 选中删除按钮，并进行点击
        driver.findElement(By.cssSelector("#artlist > div:nth-child(1) > a:nth-child(6)")).click();

        Alert alert = driver.switchTo().alert();
        alert.accept();

        // 这里进行了两次点击
        alert.accept();

        // 针对删除操作进行截图
        try {
            saveScreenShot(driver, "文章删除后截图", pathname);
        } catch (IOException e) {
            e.printStackTrace();
        }

        sleep(500);
    }
}
