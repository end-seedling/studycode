package TestClasses;

import Utils.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.io.FileHandler.createDir;

public class BlogAddTest extends TestUtils {

    // 首先引入浏览器
    public static WebDriver driver = getChromeDriver();

    static String pathname = "./src/main/screenshots/BlogAddTest";

    @BeforeAll
    public static void beforeAll() throws IOException {
        // 打开网页
        driver.get("http://localhost:8080/login.html");
        // 创建文件夹存储截图信息
        createDir(new File(pathname));
    }

    // 实现一个登录操作
    // 不能直接进入其他页面，需要进行登录判断，这里就实现一个登录方法
    public void login() {
        // 进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 因为没新开一个页面都需要进行登录，这里要针对 个人文章列表页 进行测试，所以在前置工作中需要进行一下登录操作
        // 登录
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("12345");

        // 选中登录按钮进行点击
        driver.findElement(By.cssSelector("#submit")).click();
    }

    // 登陆之后会进入到个人主页，在这里从右上角直接进入写博客页面
    @Order(1)
    @Test
    public void addBlog() {

        // 首先先进行登录操作
        login();

        // 进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // 选中写博客按钮进行点击
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)")).click();

        // 进入博客编辑页后截图
        try {
            saveScreenShot(driver,"进入博客编辑页截图", pathname);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 处于简单演示，在这里选中标题栏，直接在其中写入标题
        WebElement webElement = driver.findElement(By.cssSelector("#title"));
        // 在其中写入内容
        webElement.sendKeys("这是一个新添加的测试博客");
        // 选中发布博客按钮，点击发布
        driver.findElement(By.cssSelector("body > div.blog-edit-container > div.title > button")).click();


        // 进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // 处理 alert 确认操作
        Alert alert = driver.switchTo().alert();
        alert.dismiss();

        // 针对新添加的博客进行截图
        try {
            saveScreenShot(driver,"新添加的博客截图", pathname);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 关闭浏览器
        closeBrowser(driver);
    }
}
