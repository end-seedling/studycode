package TestClasses;

import Utils.TestUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class BlogLoginTest extends TestUtils {

    // 获取一个WebDriver实例
    public static WebDriver driver = getChromeDriver();

    // 创建一个截图存储路径
    public static String pathname = "./src/main/screenshots/BlogLoginTest/";

    // 实现在其他测试用例启动前的页面跳转操作
    @BeforeAll
    public static void beforeAll() {
        // 这里使用的是本地运行项目，所以链接为 localhost
        driver.get("http://localhost:8080/login.html");
    }

    /*
    *   检查进入的登录页面是否正确
    *   检查点，当前页面中心信息是否为 “登录”
    */
    @Order(1)
    @Test
    public void testLogin() throws InterruptedException {
        // 先进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 获取页面中心信息
        WebElement webElement = driver.findElement(By.cssSelector("body > div.login-container > div > h3"));
        String s = webElement.getText();
        boolean flag = false;
        if(s.equals("登录")) {
            flag = true;
        }
        System.out.println("进入的页面是否正确：" + flag);

        // 对进入的登录页面进行截图操作
        try {
            saveScreenShot(driver, "验证登录页面", pathname);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 截图结束后，进行强制等待后退出浏览器
        sleep(2000);

//        closeBrowser(driver);
    }


    /*
    *   检查输入用户名和密码后，登录按钮是否可用
    */
    @Order(2)
    // 开启参数化测试
    @ParameterizedTest
    @CsvSource({"zhangsan, 12345"})
    public void testLoginButton(String name, String password) {
        // 首先进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // 输入用户名和密码
        driver.findElement(By.cssSelector("#username")).sendKeys(name);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        // 获取点击按钮
        WebElement webElement = driver.findElement(By.cssSelector("#submit"));

        // 对按钮进行点击操作
        webElement.click();

        // 点击之后进行隐式等待
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        // 进入主页后进行截图操作
        try {
            saveScreenShot(driver, "验证登录按钮", pathname);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 点击之后确定当前页面是否为个人博客页面
        WebElement getMsg = driver.findElement(By.cssSelector("body > div.nav > span.title"));
        // 获取其中的文字信息
        String s = getMsg.getText();

        boolean flag = false;

        if(s.equals("我的个人主页")) {
            flag = true;
        }

        System.out.println("输入用户名和密码后，是否跳转到个人主页：" + flag);
    }



    /*
     *   针对异常登录情况进行自动化测试
     *   这里的异常情况有三种，账号密码均为 null，账号错误，密码错误
     */
    @Order(3)
    @ParameterizedTest
    @CsvSource({"zhangsan, 123456","zhang, 12345"})
    public void testLoginException(String name, String password) throws InterruptedException {

        // 将页面跳转到登录页
        driver.get("http://localhost:8080/login.html");

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 每次尝试登录之后，都需要进行一次清空操作
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();

        // 输入用户名和密码
        driver.findElement(By.cssSelector("#username")).sendKeys(name);;
        driver.findElement(By.cssSelector("#password")).sendKeys(password);

        // 获取点击按钮
        driver.findElement(By.cssSelector("#submit")).click();
        sleep(500);

        Alert alert = driver.switchTo().alert();

        String alertText = alert.getText();
        System.out.println("异常情况下的提示信息：" + alertText);

        alert.accept();
    }

    // 实现所有测试用例执行结束后的操作
    @AfterAll
    public static void tearDown() {
        closeBrowser(driver);
    }
}
