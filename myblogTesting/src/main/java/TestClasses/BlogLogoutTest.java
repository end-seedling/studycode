package TestClasses;

import Utils.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BlogLogoutTest extends TestUtils {

    // 实现对浏览器引擎获取
    public static WebDriver driver = getChromeDriver();

    // 编辑截图存储文件
    public static String pathname = "./src/main/screenshots/BlogLogoutTest/";

    // 实现前置操作
    @BeforeAll
    public static void BeforeAll() {
        // 这里需要先登录才能验证退出
        driver.get("http://localhost:8080/login.html");
    }

    // 实现一个登录操作
    // 不能直接进入其他页面，需要进行登录判断，这里就实现一个登录方法
    public void login() {
        // 进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 因为没新开一个页面都需要进行登录，这里要针对 个人文章列表页 进行测试，所以在前置工作中需要进行一下登录操作
        // 登录
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("12345");

        // 选中登录按钮进行点击
        driver.findElement(By.cssSelector("#submit")).click();

        // 点击之后进行隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Order(1)
    @Test
    public void testLogout() {

        // 进行登录操作
        login();
        // 进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 针对登录成功后的个人博客列表页实现截图
        try {
            saveScreenShot(driver,"登录成功后的个人博客列表页截图", pathname);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 选中注销按钮，点击退出
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(6)")).click();

        // 进行一下隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 进行 Alert 确定判断
        Alert alert = driver.switchTo().alert();
        alert.accept();

        // 点击注销后退出到主页面截图
        try {
            saveScreenShot(driver,"注销后退出到主页面截图", pathname);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 退出浏览器
        closeBrowser(driver);
    }
}
