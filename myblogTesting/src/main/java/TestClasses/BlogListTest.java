package TestClasses;

import Utils.TestUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class BlogListTest extends TestUtils {

    // 从集成类中调用相关浏览器引擎
    public static WebDriver driver = getChromeDriver();
    // 创建存储截图的文件夹
    public static String pathname = "./src/main/screenshots/BlogListTest";

    // 执行前置操作
    @BeforeAll
    static void beforeAll() {
        // 直接进入博客主页
        driver.get("http://localhost:8080/blog_list.html");
    }

    // 测试这里的 “查看全文按钮”
    @Order(1)
    @Test
    public void testBlogList() {
        // 这里不需要登录操作，博客主页的浏览任何人都可以直接进行

        // 这里针对博客主页进行截图操作
        try {
            saveScreenShot(driver,"博客主页截图", pathname);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 这里就查看当前第一篇博客
        // 先来获取当前博客的标题
        WebElement ListTitle = driver.findElement(By.cssSelector("#artlist > div:nth-child(1) > div.title"));
        String title = ListTitle.getText();

        // 选中查看全文按钮，点击查看
        driver.findElement(By.cssSelector("#artlist > div:nth-child(1) > a")).click();

        // 进行隐式等待
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // 进入之后尝试获取当前文章标题
        WebElement titles = driver.findElement(By.cssSelector("#title"));
        String title2 = titles.getText();

        // 断言标题是否一致
        boolean flag = false;
        if(title.equals(title2)) {
            flag = true;
        }
        System.out.println("标题是否一致：" + flag);

        // 这里再次进行截图操作
        try {
            saveScreenShot(driver,"博客主页进入详情页截图", pathname);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 下面测试的是主页中的 首页、上一页、下一页、末页 按钮
    // 测试点击下一页
    @Order(2)
    @Test
    public void testUPPageButton() {
        // 选中下一页按钮，点击
        driver.findElement(By.cssSelector("#pagediv > button:nth-child(3)")).click();
    }


    // 测试点击上一页
    @Order(3)
    @Test
    public void testDownPageButton() throws InterruptedException {
        // 选中上一页按钮，点击
        driver.findElement(By.cssSelector("#pagediv > button:nth-child(2)")).click();

        // 这里需要进行显示等待
        sleep(500);

        Alert alert = driver.switchTo().alert();
        // 在点击之前对文本进行获取
        System.out.println(alert.getText());

        alert.accept();
    }


    // 测试点击首页
    @Order(4)
    @Test
    public void testFirstPageButton2() throws InterruptedException {
        // 选中首页按钮，点击
        driver.findElement(By.cssSelector("#pagediv > button:nth-child(1)")).click();

        // 这里需要进行显示等待
        sleep(500);

        Alert alert = driver.switchTo().alert();
        // 在点击之前对文本进行获取
        System.out.println(alert.getText());

        alert.accept();
    }

    // 测试点击末页
    @Order(5)
    @Test
    public void testLastPageButton() {
        // 选中末页按钮，点击
        driver.findElement(By.cssSelector("#pagediv > button:nth-child(4)")).click();
    }

    @AfterAll
    public static void tearDown() {
        // 关闭浏览器
        closeBrowser(driver);
    }
}
