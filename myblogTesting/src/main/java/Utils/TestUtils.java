package Utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class TestUtils {
    // 在这里实现代码的复用
    public static ChromeDriver chromeDriver;

    // 创建出来驱动对象
    public static ChromeDriver getChromeDriver(){

        ChromeOptions options = new ChromeOptions();
        // 设置无头模式
        options.addArguments("--headless");

        // 判断当前驱动是否已经建立
        if(chromeDriver == null) {
            // 建立一个对象
            chromeDriver = new ChromeDriver();

            // 设定隐式等待渲染页面
            chromeDriver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        }

        return chromeDriver;
    }

    // 保存现场截图
    public static void saveScreenShot(WebDriver driver, String fileName) throws IOException {
        File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        // 将截图保存到本地位置
        FileUtils.copyFile(file, new File("F:\\Java\\Java project\\myblogTesting\\src\\main\\ScreenShot\\" + fileName + ".png"));
    }

    // 对这个截图方法进行一下重载, 可以直接传递文件目录实现图片分类
    public static void saveScreenShot(WebDriver driver, String fileName, String fileSave) throws IOException {
        File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        // 将截图保存到本地位置
        FileUtils.copyFile(file, new File(fileSave + "\\" + fileName + ".png"));
    }

    // 关闭浏览器
    public static void closeBrowser(WebDriver driver){
        if(driver != null) {
            driver.quit();
        }
    }
}
