import Utils.TestUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;

public class Test extends TestUtils {
    // 这是一个测试类
    // 测试一下能否进入到 百度首页中
    public static void main(String[] args) {
        ChromeOptions options = new ChromeOptions();
        // 允许所有操作请求
        options.addArguments("--remote-allow-origins-*");
        WebDriver webDriver = new ChromeDriver(options);
        // 打开百度首页
        webDriver.get("https://www.baidu.com");

        try {
            saveScreenShot(webDriver,"test.png");
        } catch (IOException e) {
            e.printStackTrace();
        }

        closeBrowser(webDriver);
    }
}
