import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.stream.Stream;

public class JunitTest {
    // 这里实现的是 @MethodSource("Generator") 对应的方法
    public static Stream<Arguments> Generator() {
        return Stream.of(Arguments.arguments(1, "张三"),
                Arguments.arguments(2,"李四"),
                Arguments.arguments(3,"王五"));
    }

    @Test
    void Test1() {
        System.out.println("这是 junitTest 中的 test1");
    }

    @Test
    void Test2() {
        System.out.println("这是 test 2");
    }

    // 跳过该注解下的测试
    @Disabled
//    @Test
    void Test3(){
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.baidu.com");
        // 这里故意实现一个失败的测试用例，用来观察报错现象
        webDriver.findElement(By.cssSelector("#ww")).click();
    }

    // 在所有 测试用例跑之前就先跑起来了
    @BeforeAll
    // 这里的 static 就可以看出其实这里的方法也就只会在类加载时只运行一次
    static void SetUp(){
        System.out.println(" 这是 BeforeAll 中的语句");
    }

    // 在所有的测试用列都跑了之后才进行执行
    @AfterAll
    static void TearDown(){
        System.out.println(" 这是 AfterAll 中的语句");
    }

    @BeforeEach
    void BeforeEach(){
         System.out.println("这是 BeforeEach 中的语句");
    }

    @AfterEach
    void AfterEach(){
        System.out.println("这是 AfterEach 中的语句");
    }

    // 实现单个参数添加
    // @ParameterizedTest 注解的作用是来标识该方法是一个参数化测试方法
    @ParameterizedTest
    // 然后使用类似于这里的注解来实现参数的添加
    @ValueSource(ints = {1, 2, 3})
    void Test4(int num){
        System.out.println(num);
    }

    @ParameterizedTest
    @ValueSource(strings = {"1","2","3"})
    void Test5(String number){
        System.out.println(number);
    }

    @ParameterizedTest
    //  @CsvSource 可以指定一个或多个由逗号分隔的值列表，每个列表代表一组测试参数
    //  JUnit会根据这些参数值为测试方法执行多次测试，每次使用一组不同的参数。
    @CsvSource({"1, 2, 3, ''"})
    void Test6(String x, String y, int z, String q) {
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        System.out.println(q);
        System.out.println("----------------------------");
    }

    // 这里代码运行的次数取决于当前文件中存在多少信息 结尾必须以 csv 为结尾
    @ParameterizedTest
    @CsvFileSource(resources = "Test1.csv")
    void Test7(String name){
        System.out.println(name);
    }

    // 要实现多种类型的信息读取，这里需要的是实现相关方法来执行
    @ParameterizedTest
    @MethodSource("Generator")
    void Test8(int num,String name) {
        System.out.println(num + ":" + name);
    }

}
