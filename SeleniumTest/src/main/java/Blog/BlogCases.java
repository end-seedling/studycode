package Blog;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static java.lang.Thread.sleep;

// 这里直接让这个类来继承前面创建好的启动类
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BlogCases extends InitAndEnd{
    public static Stream<Arguments> Generator() {
        return Stream.of(Arguments.arguments(
                "http://localhost:8080/blog_content.html?id=9", "博客正文", "自动化测试"));
    }

    // 输入正确的账号密码，登陆成功
//    @Test
    // 这里为了避免参数修改的问题，这里使用对应的参数化
    @Order(1)
    @ParameterizedTest
    @CsvFileSource(resources = "LoginSuccess.csv")
    void LoginSuccess(String username, String password, String list_URL) throws InterruptedException {
//        System.out.println(username + password + list_URL);

                // 打开博客登录页面
        webDriver.get("http://localhost:8080/login.html");
        // 这里将等待时间设置为显示等待，可以加快代码运行速度
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // 输入登陆账号 admin
        webDriver.findElement(By.cssSelector("#username")).sendKeys(username);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // 输入密码 123456
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        // 点击提交按钮
        webDriver.findElement((By.cssSelector("#submit"))).click();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        // 跳转到列表页
        // 获取到当前页面的 url
        // 如果 url 相等则通过，否则不通过
        String get_url = webDriver.getCurrentUrl();
        sleep(300);
        // 使用断言来判断是否通过
        Assertions.assertEquals(get_url, list_URL);
        // 列表页展现出的用户名称为 admin
        // 如果用户名为 admin 则通过 反之 不通过
//        String get_user = webDriver.findElement(By.cssSelector(""))
    }

    // 查看博客列表中的博客数量
    @Order(2)
    @Test
    void BlogList() {
        // 打开博客列表页
        webDriver.get("http://localhost:8080/myblog_list.html");
        // 获取页面上所有的博客标题对应的界面
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        int title_num = webDriver.findElements(By.cssSelector(".title")).size();
        // 获取到的元素数量不为 0 此时就说明测试通过
        Assertions.assertNotEquals(0,title_num);
    }

    // 验证查看全文功能，博客详情页校验
    // url
    // 博客标题
    // 页面 title 名称为 ”博客正文“

    // @Test
    // 因为这里要对数据进行校验，所以这里要实现参数化
    @Order(3)
    @ParameterizedTest
    @MethodSource("Generator")
    void BlogDetail(String url, String title, String blog_title) {
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        // 这里定位到第一个博客后，点击查看详情
        webDriver.findElement(By.xpath("//*[@id=\"artlist\"]/div[1]/a[1]")).click();

        // 获取当前页面的 url
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        String get_url = webDriver.getCurrentUrl();

        // 在去获取当前页面的 title
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        String get_title = webDriver.getTitle();
        // 获取博客详情页的标题
        String get_blog_title = webDriver.findElement(By.cssSelector("#title")).getText();

        //进行断言
//        Assertions.assertEquals(url,get_url);
        Assertions.assertEquals(title,get_title);
        Assertions.assertEquals(blog_title,get_blog_title);
        if(get_url.contains(blog_title)) {
            System.out.println("测试通过");
        } else {
            System.out.println("测试不通过");
        }
    }

    // 校验写博客和发布博客
    @Order(4)
    @Test()
    void EditBlog() throws InterruptedException {
        // 找到写博客按钮点击
        webDriver.findElement(By.xpath("//*[@id=\"UserLoginElement\"]/a[1]")).click();
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        // 找到输入框，点击发布
        // 通过 JS 对标题进行输入
        ((JavascriptExecutor)webDriver).executeScript("document.getElementById(\"title\").value=\"自动化测试\"\n");
        sleep(3000);
        // 点击发布
        webDriver.findElement(By.cssSelector("body > div.blog-edit-container > div.title > button")).click();
        sleep(3000);

        // 获取当前的 url
        String get_url = webDriver.getCurrentUrl();
        Assertions.assertEquals("http://localhost:8080/myblog_list.html", get_url);
    }

    // 校验已发布的博客标题
    // 校验已发布的博客时间
    @Order(5)
    @Test
    void BlogInfoChecked() {
        webDriver.get("http://localhost:8080/myblog_list.html");
        // 获取第一篇博客的标题
        String get_title = webDriver.findElement(By.cssSelector("#artlist > div:nth-child(1) > div.title")).getText();
        // 获取第一篇博客的发布时间
        String get_firstBlog_time = webDriver.findElement(By.cssSelector("#artlist > div:nth-child(1) > div.date")).getText();
        // 校验博客标题
        Assertions.assertEquals("自动化测试", get_title);
        // 如果 时间是 2024 发布就通过
        if(get_firstBlog_time.contains("2024-05-08")) {
            System.out.println("测试通过");
        }else {
            System.out.println("当前时间 " + get_firstBlog_time);
            System.out.println("测试不通过");
        }
    }

    // 需要删除这里测试产生的脏数据
    @Order(6)
    @Test
    void DeleteBlog() {
        // 打开博客列表页面
        webDriver.get("http://localhost:8080/myblog_list.html");
        // 之后点击删除文章按钮
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("#artlist > div:nth-child(1) > a:nth-child(6)")).click();
    }
}
