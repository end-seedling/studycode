package Blog;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

// 在这里单独实现一个类，就是为了记录浏览器的启动和关闭
// 以此来作为之后代码的启动和关闭接口
public class InitAndEnd {
    static WebDriver webDriver;

    @BeforeAll
    static void SetUp() {
        webDriver = new ChromeDriver();
    }

    @AfterAll
    static void TearDown() {
        webDriver.quit();
    }
}
