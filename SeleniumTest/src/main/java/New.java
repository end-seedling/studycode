import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class New {
    public static void main7(String[] args) throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.baidu.com");

        // 前面的操作都一样，找到输入框 —— 输入信息 —— 点击搜索
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("风景照片");
        webDriver.findElement(By.cssSelector("#su")).click();
        sleep(2000);
        WebElement webElement = webDriver.findElement(By.cssSelector("#s_tab > div > a.s-tab-item.s-tab-item_1CwH-.s-tab-pic_p4Uej.s-tab-pic"));

        Actions actions = new Actions(webDriver);
        actions.moveToElement(webElement).contextClick().perform();

        webDriver.findElement(By.cssSelector("#kw")).clear();
    }

    public static void main6(String[] args) throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.baidu.com");
        sleep(1000);
        // 尝试复制一个其实页面的热搜信息
        webDriver.findElement(By.xpath("//*[@id=\"hotsearch-content-wrapper\"]/li[2]/a/span[2]")).sendKeys(Keys.CONTROL,"C");
        webDriver.findElement(By.cssSelector("#kw")).sendKeys(Keys.CONTROL,"V");
        sleep(2000);
        webDriver.findElement(By.cssSelector("#su")).click();
    }

    public static void main5(String[] args) {
        // 创建出一个驱动
        WebDriver webDriver = new ChromeDriver();
        // 打开百度首页
        webDriver.get("https://www.baidu.com");
        // 再打开首页后进行 3000ms 的等待操作
        WebDriverWait wait = new WebDriverWait(webDriver,3000);
        // 这里是尝试等待网页的标题是否正常加载出来
        wait.until(ExpectedConditions.titleIs("百度一下，你就知道"));
    }

    public static void main4(String[] args) {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.baidu.com");
        // 获取 WebDriver 下访问到的网页的元素
        String title = webDriver.getTitle();

        // 设置影视等待时间与单位(这里设置的是 纳秒)
        webDriver.manage().timeouts().implicitlyWait(100, TimeUnit.MINUTES);
    }

    public static void main3(String[] args) {
        ChromeOptions options = new ChromeOptions();
        // 允许所有操作请求
        options.addArguments("--remote-allow-origins-*");
        WebDriver webDriver = new ChromeDriver(options);
        // 打开百度首页
        webDriver.get("https://www.baidu.com");
        WebElement inText = webDriver.findElement(By.cssSelector("#su"));
        WebElement text = webDriver.findElement(By.xpath("//*[@id=\"hotsearch-content-wrapper\"]/li[2]/a/span[2]"));
        System.out.println(text.getText());
        System.out.println(inText.getText());
    }

    public static void main2(String[] args) throws InterruptedException {
        ChromeOptions options = new ChromeOptions();
        // 允许所有操作请求
        options.addArguments("--remote-allow-origins-*");
        WebDriver webDriver = new ChromeDriver(options);

        // 打开百度首页
        webDriver.get("https://www.baidu.com");
        // 首先获取到输入框，并对输入框实现模拟键盘输入操作
        WebElement webElement = webDriver.findElement(By.cssSelector("#kw"));
        webElement.sendKeys("风景");
        // 此时等待三秒
        sleep(3000);
        // 在这之后，实现点击操作，点击 “百度一下”
        webDriver.findElement(By.cssSelector("#su")).click();
        // 在进行三秒的等待
        sleep(3000);
        // 实现清除输入框信息
        webElement.clear();
    }

    public static void main(String[] args) throws InterruptedException {
        int flag = 0;
        ChromeOptions options = new ChromeOptions();
        // 允许所有操作请求
        options.addArguments("--remote-allow-origins-*");
        WebDriver webDriver = new ChromeDriver(options);
        // 打开百度首页
        webDriver.get("https://www.baidu.com");
        // 这里使用的是 css 选择器
        WebElement element = webDriver.findElement(By.cssSelector("#kw"));
        element.sendKeys("风景图片");
        // 找到 “百度一下” 按钮，使用 .click 进行一次点击操作
        webDriver.findElement(By.cssSelector("#su")).click();
        // 在校验之前，这里给一个 3 秒的强制等待，给网页一个反应的时间
        sleep(3000);
        // 校验
        // 定义一个链表，在这里用于存放标签下的信息
        List<WebElement> elements = webDriver.findElements(By.cssSelector("a font"));
        // 这里使用一个 for 对列表进行遍历
        for(int i = 0; i < elements.size(); i++){
            //这里是简单测试一下这里搜索到了那些信息
            // System.out.println(elements.get(i).getText());
            // 如果返回的结果包含 “风景” ，此时则证明测试通过，否则不通过
            if(elements.get(i).getText().equals("风景")) {
                flag = 1;
                System.out.println(elements.size());
                System.out.println("测试通过");
                break;
            }
        }
        if(flag == 0) {
            System.out.println("测试不通过");
        }
    }
}

