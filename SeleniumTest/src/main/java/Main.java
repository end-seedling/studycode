import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {
        //test1();
        //test2();
        // 实现一个方法，该方法的操作是使用 submit 点击由 a 标签设置的 “新闻” 链接
//        test3();
//        test4();

        //test6();
//        test7();
//        test8();
//        test9();

//        page1();
//        page2();
//        page3();

        // 针对 alert 进行操作
//        page4();
//        page5();

//        test10();
//        test11();
//        test12();
    }

    // 实现截图操作
    private static void test12() throws InterruptedException, IOException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.baidu.com");
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("风景图片");
        webDriver.findElement(By.cssSelector("#su")).click();
        sleep(4000);
        // 这里要实现截图操作，就需要进行强制转换
        // 获取当前 WebDriver 打开的最上层页面
        File file = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
        // 将截图得到的信息保存到本地位置
        FileUtils.copyFile(file,new File("H://jietu.png"));
    }

    private static void test11() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.baidu.com");
        webDriver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).click();
        sleep(2000);
        // 通过 getWindowHandles 获取当前所有的窗口句柄
        // getWindowHandle 获取的只是当前 get 之后打开的句柄
        Set<String> handles = webDriver.getWindowHandles();
        String target_handle = "";
        for (String handle:handles){
            // 通过循环遍历，就可以拿到最后的一个 句柄 信息。
            target_handle = handle;
        }
        sleep(2000);
        // 实现切换到最后打开的新的页面
        webDriver.switchTo().window(target_handle).close();
//        webDriver.findElement(By.cssSelector("#ww")).sendKeys("新闻联播");
//        webDriver.findElement(By.cssSelector("#s_btn_wr")).click();
    }

    // 实现关闭网页，观察关闭网页的两种操作之间的区别
    private static void test10() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.baidu.com");
        webDriver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).click();
        sleep(3000);
        // 关闭整个浏览器
//        webDriver.quit();
        // 关闭 get 出来的页面
        webDriver.close();
    }

    private static void page5() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("file:///F:/Java/Java%20project/Software%20testing/SeleniumTest/src/main/page/test5.html");
        webDriver.findElement(By.cssSelector("input")).sendKeys("H:\\网页制作技术");
    }

    private static void page4() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("file:///F:/Java/Java%20project/Software%20testing/SeleniumTest/src/main/page/tset4.html");
        webDriver.findElement(By.cssSelector("button")).click();
        sleep(2000);
        // alert 弹窗的一个取消
        webDriver.switchTo().alert().dismiss();
        sleep(2000);
        // 之后再次点击按钮
        webDriver.findElement(By.cssSelector("button")).click();
        // 输入 “猫猫”
        webDriver.switchTo().alert().sendKeys("猫猫");
        sleep(2000);
        // 在针对弹窗点击确认
        webDriver.switchTo().alert().accept();
    }

    private static void page3() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("file:///F:/Java/Java%20project/Software%20testing/SeleniumTest/src/main/page/test3.html");
        WebElement webElement = webDriver.findElement(By.cssSelector("#ShippingMethod"));
        Select select = new Select(webElement);
        // 通过 下标 来实现查找 （这里的下标是从 0 开始的）
        select.selectByIndex(3);
        // 通过 value 进行查询
        sleep(3000);
        select.selectByValue("12.51");
    }

    // 多窗口定位
    private static void page2() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("file:///F:/Java/Java%20project/Software%20testing/SeleniumTest/src/main/page/test2.html");
        // 因为 click 按钮在 ifram 标签内部，这里就需要使用 switchTo() 进行切换操作
        webDriver.switchTo().frame("f1");
        webDriver.findElement(By.cssSelector("body > div > div > a")).click();
    }

    private static void page1() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("file:///F:/Java/Java%20project/Software%20testing/SeleniumTest/src/main/page/test1.html");
        // 将找到的元素依照链表的形式进行存储
        // 这里添加了一个隐式等待
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.DAYS);
        List<WebElement> webElements = webDriver.findElements(By.cssSelector("input"));
        for (int i = 0; i < webElements.size(); i++){
            // 当前如果元素的 type 值为 checkbox 就进行点击
            // 需要注意的是 这里的 getAttribute 返回值就是当前 type 对应的名称
            if(webElements.get(i).getAttribute("type").equals("checkbox")) {
                webElements.get(i).click();
            }
            // 否则不进行任何操作
        }
    }

    private static void test9() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.baidu.com");
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("风景照片");
        webDriver.findElement(By.cssSelector("#su")).click();
        sleep(2000);
        // 找到图片超链接按钮
        WebElement webElement = webDriver.findElement(By.cssSelector("#s_tab > div > a.s-tab-item.s-tab-item_1CwH-.s-tab-pic_p4Uej.s-tab-pic"));
        // 实现右击操作
        Actions actions = new Actions(webDriver);
        sleep(3000);
        // 对鼠标进行移动操作并且实现点击
        actions.moveToElement(webElement).contextClick().perform();
    }

    // 模拟键盘事件
    private static void test8() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        // 打开百度首页
        webDriver.get("https://www.baidu.com");
        // 搜索 “风景照片”
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("风景照片");
        // ctrl + A
        webDriver.findElement(By.cssSelector("#kw")).sendKeys(Keys.CONTROL,"A");
        sleep(2000);
        // ctrl + X
        webDriver.findElement(By.cssSelector("#kw")).sendKeys(Keys.CONTROL,"X");
        sleep(2000);
        // ctrl + V
        webDriver.findElement(By.cssSelector("#kw")).sendKeys(Keys.CONTROL,"V");
        sleep(2000);
    }

    // 实现控制浏览器操作
    private static void test7() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        // 打开百度首页
        webDriver.get("https://www.baidu.com");
        // 搜索 “风景照片”
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("风景照片");
        // 强制等待 3 秒
        sleep(3000);
        webDriver.findElement(By.cssSelector("#su")).click();
        // 浏览器后退
        sleep(3000);
        webDriver.navigate().back();
        // 页面刷新
        sleep(3000);
        webDriver.navigate().refresh();
        // 强制等待 3 秒
        sleep(3000);
        // 浏览器前进
        webDriver.navigate().forward();
        // 实现滚动条滚动
        // 这里要执行的是一个 js 脚本，需要进行一个强制类型转换
        sleep(3000);

        ((JavascriptExecutor)webDriver).executeScript("document.documentElement.scrollTop=10000");


        // 实现浏览器最大化
        sleep(2000);
        webDriver.manage().window().maximize();
        sleep(2000);
        // 实现一个全屏的操作
        webDriver.manage().window().fullscreen();
        sleep(2000);
        // 设定固定宽高的像素情况
        webDriver.manage().window().setSize(new Dimension(600,1000));
    }

    // 判断标签是否可以被点击
    private static void test6() {
        // 创建出一个驱动
        WebDriver webDriver = new ChromeDriver();
        // 打开百度首页
        webDriver.get("https://www.baidu.com");
        // 再打开首页后进行 3000ms 的等待操作
        WebDriverWait wait = new WebDriverWait(webDriver,1);
//        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#bottom_layer > div > p:nth-child(8) > span")));
        wait.until(ExpectedConditions.titleIs("百度一下，你就知道"));
    }

    private static void test4() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        WebDriver webDriver = new ChromeDriver(options);
        webDriver.get("https://www.baidu.com/");
        String button_value = webDriver.findElement(By.cssSelector("#su")).getAttribute("value");
        System.out.println(button_value);
        if(button_value.equals("百度一下")) {
            System.out.println("测试通过");
        } else {
            System.out.println(button_value);
            System.out.println("测试不通过");
        }
    }

    // 实现 submit 的提交操作
    private static void test3(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        WebDriver webDriver = new ChromeDriver(options);
        webDriver.get("https://www.baidu.com/");
        webDriver.findElement(By.xpath("//*[@id=\"s-top-left\"]/a[4]")).submit();
//        webDriver.findElement(By.xpath("//*[@id=\"s-top-left\"]/a[4]")).click();
    }

    // 实现清空操作
    private static void test2() throws InterruptedException {
        ChromeOptions options = new ChromeOptions();
        // 允许所有操作请求
        options.addArguments("--remote-allow-origins-*");
        WebDriver webDriver = new ChromeDriver(options);
        // 打开百度首页
        webDriver.get("https://www.baidu.com");
        sleep(3000);
        // 随便输入了一个信息
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("hahaha");
        // 点击了一下百度搜索
        webDriver.findElement(By.cssSelector("#su")).click();
        sleep(3000);
        // 实现对于输入框的清除
        webDriver.findElement(By.cssSelector("#kw")).clear();
    }

    // 简单对于输入框进行输入数据进行测试
    private static void test1() throws InterruptedException {
        int flag = 0;
        ChromeOptions options = new ChromeOptions();
        // 允许所有操作请求
        options.addArguments("--remote-allow-origins-*");
        WebDriver webDriver = new ChromeDriver(options);
        // 打开百度首页
        webDriver.get("https://www.baidu.com");
        // 根据对应的 css 类来找到百度的搜索框。这里的返回值类型是 WebElement
        WebElement element = webDriver.findElement(By.cssSelector("#kw"));
//        WebElement element = webDriver.findElement(By.xpath("//*[@id=\"kw\"]"));
        // 在对话框中输入要搜索的信息
        element.sendKeys("风景照片");
        // 在将找到空白框后，需要实现一下点击搜索操作
        webDriver.findElement(By.cssSelector("#su")).click();
        sleep(3000);
        // 校验
        // 找到搜索的结果 将搜索到的信息存储到链表中 “这里查找到的数据类型是 WebElement”
        List<WebElement> elements = webDriver.findElements(By.cssSelector("a em"));
        // 这里使用一个 for 对列表进行遍历
        for(int i = 0; i < elements.size(); i++){
            //这里是简单测试一下这里搜索到了那些信息
            // System.out.println(elements.get(i).getText());
            // 如果返回的结果包含 “风景” ，此时则证明测试通过，否则不通过
            if(elements.get(i).getText().equals("风景")) {
                flag = 1;
                System.out.println("测试通过");
                break;
            }
        }
        if(flag == 0) {
            System.out.println("测试不通过");
        }
    }
}
