// 这个类实现的是测试套件

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

@Suite
// 按照这里的类顺序来执行测试用例，通过 class 来进行测试用例的运行
//@SelectClasses({JunitTest.class,JunitTest01.class})
// 还可以指的一个包来进行运行
@SelectPackages(value = {"package01","package02"})
public class RunSuite {

}
