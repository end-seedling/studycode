import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

// 这个注解可以开启对于该类内部的方法进行顺序的调整
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
// 还有一个注解可以开启类中方法随机执行的操作
//@TestMethodOrder(MethodOrderer.Random.class)
public class JunitTest01 {

    /////////////////////////////////////////////////////
    // 在这一块代码中会发现这里的执行顺序无论方法的位置如何变动，这里代码执行的顺序都将会是固定的
    // 这是因为 Junit5 中的核心算法固定的
    // 这里的这个 @Order 注解中的数字就是设置方法执行的顺序
//    @Order(2)
    @Test
    void B () {
        System.out.println("这是方法 B");
    }
//    @Order(4)
    @Test
    void Test01 () {
        System.out.println("这是 Test01 测试用例");
    }
//    @Order(3)
    @Test
    void Test02 () {
        System.out.println("这是 Test02 测试用例");
    }

//    @Order(1)
    @Test
    void A () {
        System.out.println("这是 A 方法");
    }
    /////////////////////////////////////////////////////

    // 断言 使用
    @ParameterizedTest
    @ValueSource(ints = {1})
    void test03(int num) {
//        System.out.println("这是 Test03 方法");
        System.out.println(num);
        // 这里是一个实现 断言相等 的方法调用，判断这里的 num 值是否符合指定的值(即这里的 1)
//        Assertions.assertEquals(1, num);
        // 这里实现的是一个 断言不相等 的方法调用
//        Assertions.assertNotEquals(2,num);
        String str1 = "null2";
        String str2 = null;
        // 这里实现的是 断言为null 的方法调用
        Assertions.assertNull(str1);
    }
}
