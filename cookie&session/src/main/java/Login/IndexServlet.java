package Login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/index")
public class IndexServlet extends HttpServlet {
    //通过重定向，浏览器发送的会是一个 get 请求
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 首先判断用户的登录状态
        // 如果还没有登录，就请求先登录
        // 已经登录了，就根据当前会话中的用户名，显示到页面上

        //首先获取当的会话，并不创建新的会话
        // 如果会话存在，则表明当前的用户已经登录了
        // 如果会话不存在，则表名当前用户处于未登录状态
        HttpSession session = req.getSession(false);
        if(session == null){
            //服务器信息反馈
            System.out.println("用户未登录");
            // 此时进行重定向，定向到登录页面
            resp.sendRedirect("login.html");
            return;
        }
        // 从前面的会话中获取到用户信息
        String username = (String) session.getAttribute("username");
        // 构造一个页面
        // 设定页面输出的格式
        resp.setContentType("text/html;charset=utf8");
        // 在页面输出信息
        resp.getWriter().write("欢迎 "+ username + " 登录！");
    }
}
