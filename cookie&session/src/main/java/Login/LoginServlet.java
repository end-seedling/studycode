package Login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取到前端页面提交的用户名以及密码信息
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        // 在获取到相应信息后，就需要对信息进行核对。
        // 正常情况下是需要数据库进行存储信息,此处为了方便就直接将用户信息写死
        // 这里约定用户有 zhangsan 和 lisi
        // 密码都为 123
        if(!username.equals("zhangsan") && !username.equals("lisi")){
            //登录失败
            //后端反馈登录失败信息
            System.out.println("登录失败，请重新登录");
            //进行页面的重定向，将页面返回到登录页
            resp.sendRedirect("login.html");
            return;
        }
        if(!password.equals("123")){
            //登录失败
            //后端反馈登录失败信息
            System.out.println("登录失败，请重新登录");
            //进行页面的重定向，将页面返回到登录页
            resp.sendRedirect("login.html");
            return;
        }
        //登陆成功
        // 1. 创建出一个会话
        // 这里设定的 true 是为了判读是否需要创建一个新的 会话
        HttpSession session = req.getSession(true);
        // 2，将当前用户的用户名保存到会话中
        session.setAttribute("username",username);
        // 3，重定向到主页面
        resp.sendRedirect("index");
    }
}
