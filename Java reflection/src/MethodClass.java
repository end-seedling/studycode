import java.lang.reflect.Method;

public class MethodClass {
    // 实现通过反射获取到 Student 类中的成员方法并且调用

    public static void main(String[] args) throws Exception{
        // 首先，通过反射获取到 student 类
        Class stuClass = Class.forName("Student");
        // 创建出一个数组
        Method[] methodArray = null;

         //首先获取所有的公有的成员方法
        System.out.println("********获取所有的“公有”方法********");
        methodArray = stuClass.getMethods();
        for (Method m : methodArray) {
            System.out.println(m);
        }

        // 获取所有的方法，包括 私有方法 在内
        System.out.println("********获取所有类型的 成员方法，包括私有类型********");
        methodArray = stuClass.getDeclaredMethods();
        for (Method m : methodArray) {
            System.out.println(m);
        }

        // 获取公有的 show1 方法
        System.out.println("********获取所有的“公有”的 show1 方法********");
        Method m = stuClass.getMethod("show1", String.class);
        System.out.println("这是 show1 方法: " + m);
        // 实例化 student 类
        Object obj1 = stuClass.getConstructor().newInstance();
        // 为 show1 方法添加上参数
        m.invoke(obj1,"这是成员方法 String 参数的测试");

        // 获取私有的 show2 方法
        System.out.println("********获取所有的“私有”方法 show4 ********");
        Method pm = stuClass.getDeclaredMethod("show4", int.class);
        System.out.println(pm);
        // 要调用这个方法就需要解除私有限定
        pm.setAccessible(true);
        Object obj2 = stuClass.getConstructor().newInstance();
        Object re = pm.invoke(obj2, 100);
        System.out.println("私有方法 String 的返回值： " + re);
    }
}
