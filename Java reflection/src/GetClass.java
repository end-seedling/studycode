public class GetClass {
//    这个类实现的是如何获取 Class 对象的三种方式
    public static void main(String[] args){

        // 第一种：通过 Object ——> getClass()
        Student stu1 = new Student();   // 通过 new 来产生一个 Student 对象，一个 Class 对象
        Class stuClass = stu1.getClass();   // 这里则是通过这个对象中的 get 操作来获取到 Class 对象 (当然也要被 Class 类型进行接受)
        System.out.println(stuClass.getName());     // 打印当前 Class(类) 的类名称

        // 第二种：通过 类 的 “静态” class 属性来获取
        Class ints = Integer.class;     // Integer 类型的 “静态” class 属性也是可以被获取的
        Class stuClass2 = Student.class;
        System.out.println("这里是 Integer ： " + ints);
        System.out.println("这里是 stuClass2： " + stuClass2);  // 可以看到这里获取到的是 包名 + 类名

        // 第三种： 通过 class 类的静态方法 forName(String className) 获取
        try {
            Class stuClass3 = Class.forName("Student");
            Class Int = Class.forName("java.lang.Integer");     // 这里的 String 类型字符串是要通过包名开始写到详细的类名

            System.out.println("这是 Student 类： " + stuClass3);
            System.out.println("这是 Integer 类： " + Int);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
