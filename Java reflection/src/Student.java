public class Student {

//    ------在这里定义出一些字段------
    public String name;
    public String name02;
    protected int age;
    char sex;
    private String phoneNUM;

    // 这里重写一下 toString 方法
    @Override
    public String toString() {
        return "Student [name=" + name + ", age=" + age + ", sex=" + sex
                + ", phoneNum=" + phoneNUM + "]";
    }

//    ------这里创建出一个 Student 类来便于反射获取并使用------

    // 没有属性的带有一个 String 参数的构造方法
    Student (String str) {
        System.out.println("没有属性的带有 String 参数的构造方法 str = " + str);
    }

    // 公共类型的构造方法
    public Student() {
        System.out.println("没有参数的构造方法（public 类型）");
    }

    // 包含一个参数的构造方法
    public Student(char name) {
        System.out.println("（包含参数的 public 类型方法）姓名：" + name);
    }

    // 有多个参数的构造方法
    public Student(String name, int age) {
        System.out.println("（公有的包含多个参数的构造方法）姓名：" + name + "年龄：" + age);
    }

    // 收到保护的构造方法
    protected Student(boolean n) {
        System.out.println("这是收到保护的构造方法 n = " + n);
    }

    // 私有的构造方法
    private Student(int age) {
        System.out.println("这是私有的构造方法 年龄：" + age);
    }

//      创建 Student 类中的成员方法
    public void show1(String s) {
        System.out.println("调用了：共有的，包含 String 类型的 show1() 方法 s: " + s);
    }

    protected void show2() {
        System.out.println("调用了：受保护的，无参数的 show2() 方法");
    }

    void show3() {
        System.out.println("调用了：默认的，没有参数的 show3()");
    }

    private String show4(int age) {
        System.out.println("调用了：私有的，带返回值的，int 参数的 show4()： age = " + age);
        return "return show04()";
    }

//    ------通过反射运行配置文件内容------
    public void show(int a) {
        System.out.println("this is show()" + a);
    }

//     ------设置一个 main 方法------
    public static void main(String[] args) {
        System.out.println("Student 中的 main 方法执行了！！");
    }
}
