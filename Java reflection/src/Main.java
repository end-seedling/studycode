import java.lang.reflect.Method;

public class Main {
//    这个类实现的是反射 Main 方法

    public static void main(String[] args) throws Exception{
        // 首先获取到 Student 类
        Class stu = Class.forName("Student");

        // 获取 main 方法
        Method getMain = stu.getMethod("main", String[].class);
        System.out.println(getMain);

        // 调用 main 方法
        getMain.invoke(null, (Object) new String[]{});
    }
}
