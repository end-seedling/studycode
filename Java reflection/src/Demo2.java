import java.lang.reflect.Method;
import java.util.ArrayList;

public class Demo2 {
//    这个类实现的是通过反射翻越泛型检查
    public static void main(String[] args) throws Exception{
        ArrayList<String> str = new ArrayList<>();
        str.add("aaa");
        str.add("bbb");
//        str.add(100);

        // 在这里的 str 中只能插入 String 类型的数据，但是通过反射，就可以将 100 这个 int 类型的元素添加进去
        // 这样就做到了跳过 泛型检查
        Class strClass = str.getClass();    // 这里是要找到 str 的字节码
        Method m = strClass.getMethod("add", Object.class);
        // 调用其中的 add 方法，并且向其中添加整形类型的数据
        m.invoke(str, 100);

        // 遍历参数
        for (Object o : str) {
            System.out.println(o);
        }
    }
}
