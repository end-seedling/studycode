import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.Properties;

public class Demo {
    // 首先实现一个获取 test.txt 文件的方法
    public static String getValue(String key) throws Exception{
        Properties pro = new Properties();      // 获取配置文件的对象
        FileReader in = new FileReader("src/txtpackage/test.txt");     // 获取输入流
        pro.load(in);   // 将流加载到配置文件对象中
        in.close();
        return pro.getProperty(key);    // 根据 key 返回 value 的值
    }


//    通过反射运行配置文件内容
    public static void main(String[] args) throws Exception{
        // 首先通过反射获取 Class 对象
        Class stu = Class.forName("Student");

        // 获取到  stu 中的 show 方法
        Method m = stu.getMethod(getValue("methodName"), int.class);   // 这里的 value 值对应的就是 show

        // 调用 show 方法
        m.invoke(stu.getConstructor().newInstance(), 1);
    }
}
