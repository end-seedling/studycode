import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class GetConstructors {
//    这个方法实现的是，通过反射的方式获取到相关的构造方法

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        // 1.首先需要加载 Class 类对象
        // 这里通过最常用的 forName 方法来获取到类对象
        Class stu = Class.forName("Student");
        // 创建 Construct 类型的数组，用于接受后续获取到的实例化对象
        Constructor[] conArray = null;

        // 获取所有的 public 类型的构造方法
//        System.out.println("********所有的公有的构造方法********");
//        conArray = stu.getConstructors();
//        for (Constructor c: conArray) {
//            System.out.println(c);
//        }


//        // 获取所有的构造方法(包括：私有、受保护、默认、公有)
//        System.out.println("********获取多有的构造方法(包括：私有、受保护、默认、公有)********");
//        conArray = stu.getDeclaredConstructors();
//        for (Constructor c: conArray) {
//            System.out.println(c);
//        }

        // 获取公有的，没有参数的构造方法
//        System.out.println("********获取公有的，没有参数的构造方法********");
//        Constructor con = stu.getConstructor(null);
//        System.out.println(con);


        // 尝试获取私有的包含参数的构造方法
        System.out.println("********获取私有的构造方法，并调用********");
        Constructor con2 = stu.getDeclaredConstructor(int.class);
        System.out.println("私有的构造方法" + con2);

/*----------------------------------------------------------*/
        // 通过 Object 调用了 Student 中获取到 con 中的方法
        // 这里就类似于 Student obj = new Student
//        Object obj2 = stu.newInstance();
/*----------------------------------------------------------*/

        // 这就相当于单独的 创建并且调用了通过getDeclaredConstructor获取的构造方法
        con2.setAccessible(true);   // 暴力访问，直接忽略访问修饰符
        con2.newInstance(100);
    }
}
